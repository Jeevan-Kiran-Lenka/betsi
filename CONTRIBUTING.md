# Contributing to Betsi

Thanks for contributing to Betsi!  Here are some guidelines for
contributing to this project.

## How to report bugs

Please open issues at our [Gitlab
repository](https://gitlab.com/librespacefoundation/polaris/betsi/-/issues),
with steps to reproduce the problem you're experiencing.

## Code standards

Betsi follows the [Google style docstring
standards](https://sphinxcontrib-napoleon.readthedocs.io/en/latest/example_google.html).
